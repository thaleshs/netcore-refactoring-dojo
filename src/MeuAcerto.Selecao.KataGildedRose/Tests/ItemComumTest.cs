﻿using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemComumTest
    {
        [Theory]
        [InlineData(0, 2)]
        [InlineData(-1, 2)]        
        public void AtualizarQualidade_PrazoDeVendaEsgotado_QualidadeDiminuiEmDobro(int prazoDeVenda, int qualidade)
        {
            var item = new ItemEspecializado { Nome = "Comum", PrazoParaVenda = prazoDeVenda, Qualidade = qualidade, TipoItem = TipoItem.Comum};

            item.AtualizarQualidade();

            Assert.Equal(qualidade - 2, item.Qualidade);
        }

        [Theory]
        [InlineData(1, 2)]
        [InlineData(1, 1)]        
        public void AtualizarQualidade_DiminuirQualidade(int prazoDeVenda, int qualidade)
        {
            var item = new ItemEspecializado { Nome = "Comum", PrazoParaVenda = prazoDeVenda, Qualidade = qualidade, TipoItem = TipoItem.Comum };

            item.AtualizarQualidade();

            Assert.Equal(qualidade - 1, item.Qualidade);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(0, 1)]
        [InlineData(1, 0)]
        public void AtualizarQualidade_DiminuirQualidade_QualidadeNaoPodeSerNegativo(int prazoParaVenda, int qualidade)
        {
            Item item = new ItemEspecializado { Nome = "Comum", PrazoParaVenda = prazoParaVenda, Qualidade = qualidade, TipoItem = TipoItem.Comum };

            item.AtualizarQualidade();

            Assert.True(item.Qualidade >= 0);
        }
    }
}
