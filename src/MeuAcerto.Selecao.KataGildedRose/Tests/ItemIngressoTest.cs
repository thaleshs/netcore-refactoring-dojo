﻿using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemIngressoTest
    {
        [Fact]
        public void AtualizarQualidade_PrazoDeVendaVencido()
        {
            var item = new ItemEspecializado { Nome = "Ingresso", PrazoParaVenda = 0, Qualidade = 50, TipoItem = TipoItem.Ingresso };

            item.AtualizarQualidade();

            Assert.Equal(0, item.Qualidade);
        }

        [Theory]
        [InlineData(20, 3)]
        public void AtualizarQualidade_AumentarQualidade(int prazoDeVenda, int qualidade)
        {
            var item = new ItemEspecializado { Nome = "Ingresso", PrazoParaVenda = prazoDeVenda, Qualidade = qualidade, TipoItem = TipoItem.Ingresso };

            item.AtualizarQualidade();

            Assert.Equal(qualidade + 1, item.Qualidade);
        }

        [Theory]
        [InlineData(10, 3)]
        public void AtualizarQualidade_AumentarQualidade_PrazoIgualMenorQueDez(int prazoDeVenda, int qualidade)
        {
            var item = new ItemEspecializado { Nome = "Ingresso", PrazoParaVenda = prazoDeVenda, Qualidade = qualidade, TipoItem = TipoItem.Ingresso };

            item.AtualizarQualidade();

            Assert.Equal(qualidade + 2, item.Qualidade);
        }

        [Theory]
        [InlineData(5, 3)]
        public void AtualizarQualidade_AumentarQualidade_PrazoIgualMenorQueCinco(int prazoDeVenda, int qualidade)
        {
            var item = new ItemEspecializado { Nome = "Ingresso", PrazoParaVenda = prazoDeVenda, Qualidade = qualidade, TipoItem = TipoItem.Ingresso };

            item.AtualizarQualidade();

            Assert.Equal(qualidade + 3, item.Qualidade);
        }

        [Theory]
        [InlineData(2, 50)]
        public void AtualizarQualidade_AumentaQualidade_NaoPodeSerMaiorQueCinquenta(int prazoParaVenda, int qualidade)
        {
            var item = new ItemEspecializado { Nome = "Ingresso", PrazoParaVenda = prazoParaVenda, Qualidade = qualidade, TipoItem = TipoItem.Ingresso };

            item.AtualizarQualidade();

            Assert.Equal(50, item.Qualidade);
        }
    }
}
