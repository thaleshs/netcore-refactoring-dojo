﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            for (int i = 0; i < Itens.Count; i++)
            {
                Itens[i].AtualizarQualidade();                
            }
        }
    }
}
